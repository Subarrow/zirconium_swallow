-module(zirconium_nr_lift_data).

-include_lib("zirconium_swallow/include/nr_lift.hrl").

-export([lift_to_record/1, record_to_map/1]).

-spec lift_type(binary()) -> lift.
lift_type(<<"Lift">>) ->
    lift.

-spec lift_status(binary()) -> available | unknown | not_available.
lift_status(<<"Available">>) ->
    available;
lift_status(<<"Unknown">>) ->
    unknown;
lift_status(<<"Not Available">>) ->
    not_available.

-spec bool(binary() | boolean()) -> boolean().
bool(<<"true">>) ->
    true;
bool(<<"false">>) ->
    false;
bool(Bool) when is_boolean(Bool) ->
    Bool.

-spec datetime(binary() | null) -> calendar:datetime() | null.
datetime(<<Year:4/binary, "-", Month:2/binary, "-", Day:2/binary, "T", Hour:2/binary, ":", Minute:2/binary, ":", Second:2/binary, ".", _Fraction:3/binary, "Z">>) ->
    {
        {binary_to_integer(Year), binary_to_integer(Month), binary_to_integer(Day)},
        {binary_to_integer(Hour), binary_to_integer(Minute), binary_to_integer(Second)}
    };
datetime(null) -> null.

station(null) ->
    {null, null};

station(#{
    <<"crsCode">> := StationCrs,
    <<"name">> := StationName
}) ->
    {StationCrs, StationName}.

-spec lift_to_record(map()) -> #nr_lift{}.
lift_to_record(#{
    <<"alternateName">> := AlternateName,
    <<"blockId">> := BlockId,
    <<"blockTitle">> := BlockTitle,
    <<"customerFacingFlag">> := IsCustomer,
    <<"engineerOnSite">> := IsEngineerOnSite,
    <<"inCommissionFlag">> := IsCommission,
    <<"independent">> := IsIndependent,
    <<"isolated">> := IsIsolated,
    <<"operationalStatus">> := OperationalStatus,
    <<"sensorId">> := SensorId,
    <<"station">> := StationRaw,
    <<"status">> := Status,
    <<"timeToFix">> := TimeToFix,
    <<"type">> := Type,
    <<"updatedTime">> := UpdatedTime,
    <<"uprn">> := UPRN
}) ->
    {StationCrs, StationName} = station(StationRaw),
    #nr_lift{block_id=BlockId, type=lift_type(Type), updated_time=datetime(UpdatedTime), status=lift_status(Status),
    operational_status = OperationalStatus, alternate_name = AlternateName,
    uprn = UPRN, block_title = BlockTitle, sensor_id = SensorId,
    station_crs = StationCrs, station_name = StationName, time_to_fix = datetime(TimeToFix),
    is_customer = bool(IsCustomer), is_engineer_on_site = bool(IsEngineerOnSite), is_isolated = bool(IsIsolated),
    is_commission = bool(IsCommission), is_independent = bool(IsIndependent)
}.

record_to_map(Record = #nr_lift{}) ->
    [nr_lift | Remainder] = tuple_to_list(Record),
    maps:from_list(lists:zip(record_info(fields, nr_lift), Remainder)).