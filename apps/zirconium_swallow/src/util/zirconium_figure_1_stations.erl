-module(zirconium_figure_1_stations).

-include_lib("zirconium_swallow/include/figure_1_stations.hrl").

-export([init_figure_1_stations/0, is_figure_1_station/1]).

init_figure_1_stations() ->
    ets:new(?MODULE, [public, named_table]),
    ets:insert(?MODULE, [{Station, true} || Station <- ?FIGURE_1_STATIONS]).

is_figure_1_station(CRS) when is_binary(CRS) ->
    case ets:lookup(?MODULE, CRS) of
        [{CRS, true}] ->
            true;
        [] ->
            false
    end.