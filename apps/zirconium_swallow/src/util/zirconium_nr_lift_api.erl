-module(zirconium_nr_lift_api).

-export([ping/1, lifts_and_escalators/2, lifts_and_escalators/3]).

-define(BASE_URL, <<"https://data.networkrail.co.uk/api/stations/v1.0/">>).

ping(Credentials) ->
    authorised_get(<<"ping">>, Credentials).

lifts_and_escalators(Credentials, Target) ->
    lifts_and_escalators(Credentials, Target, []).

lae_args(Url, Args) ->
    QueryArgs = lists:map(
        fun 
            (passenger_only) -> {<<"customerFacingOnly">>, <<"true">>};
            (commission_only) -> {<<"inCommissionOnly">>, <<"true">>};
            (lift_only) -> {<<"assetType">>, "Lift"};
            (_) -> error("Argument not passenger_only, commission_only, lift_only")
        end,
        lists:uniq(Args)
    ),
    case QueryArgs of
        <<>> -> Url;
        QueryArgs -> 
            EncodedArgs = hackney_url:qs(QueryArgs),
            <<Url/binary, "?", EncodedArgs/binary>>
    end.

lifts_and_escalators(Credentials, all, Args) ->
    authorised_get(lae_args(<<"stations/all/lifts-and-escalators">>, Args), Credentials);

lifts_and_escalators(Credentials, CRS, Args) ->
    CRSBin = iolist_to_binary(CRS),
    authorised_get(lae_args(<<"stations/crs-codes/", CRSBin/binary, "/lifts-and-escalators">>, Args), Credentials).

authorise(#{client_id := ClientID, client_secret := ClientSecret}) ->
    [
        {<<"client_id">>, ClientID},
        {<<"client_secret">>, ClientSecret},
        {<<"x-correlation-id">>, <<"a">>},
        {<<"Accept">>, "application/json"}
    ].

authorised_get(Url, Credentials) ->
    Headers = authorise(Credentials),
    case hackney:request(get, <<?BASE_URL/binary, Url/binary>>, Headers, "", [with_body]) of
        {ok, 200, _Headers, Body} ->
            {ok, Body};
        Error = {error, _Term} ->
            Error
        end.