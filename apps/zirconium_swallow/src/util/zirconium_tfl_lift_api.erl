-module(zirconium_tfl_lift_api).

-define(BASE_URL, <<"https://api.tfl.gov.uk/">>).

-export([lift_disruptions/0]).

lift_disruptions() ->
    url_get(<<"Disruptions/Lifts/v2/">>).

url_get(Url) ->
    case hackney:request(get, <<?BASE_URL/binary, Url/binary>>, [], "", [with_body]) of
        {ok, 200, _Headers, Body} ->
            {ok, Body};
        Error = {error, _Term} ->
            Error
        end.