-module(zirconium_store).

-behaviour(gen_server).

-export([init/1, handle_cast/2, handle_info/2, handle_call/3]).

-export([start_link/1]).

start_link(_) ->
    gen_server:start_link(?MODULE, nil, []).

init(State) ->
    % Register module with our own name
    true = register(?MODULE, self()),
    {ok, State}.

handle_info({Feed, fail}, State) ->
    logger:error("Feed ~p poller reports failure", [Feed]),
    {noreply, State};

handle_info({nr_lift, Results}, State) ->
    {atomic, _Ret} = mnesia:transaction(fun() ->
        lists:map(fun mnesia:write/1, Results)
    end),

    {noreply, State};

handle_info(_Message, _State) ->
    error("Unsupported message").

handle_call(_Message, _Sender, _State) ->
    error("Unsupported call").

handle_cast(_Message, _State) ->
    error("Unsupported cast").
