-module(zirconium_poller).

-behaviour(gen_server).
-export([init/1, handle_cast/2, handle_info/2, handle_call/3]).

-export([start_link/1]).
-export([fetch/1]).

start_link(_) ->
    gen_server:start_link(?MODULE, nil, []).

fetch(Credentials) ->
    {ok, ResponseBody} = zirconium_nr_lift_api:lifts_and_escalators(Credentials, all, [passenger_only, commission_only, lift_only]),
    #{
        <<"header">> := #{
            <<"apiName">> := <<"e-nr-stations">>,
            <<"apiVersion">> := <<"v1.0">>,
            <<"correlationId">> := _CorrelationId
        },
        <<"data">> := #{
            <<"results">> := _ResultsCount,
            <<"resultSet">> := ResultSet
        }
    } = jsone:decode(ResponseBody),

    ?MODULE ! {got, lists:map(fun (Result) -> zirconium_nr_lift_data:lift_to_record(Result) end, ResultSet)}.

get_creds() ->
    {ok, #{credentials := Credentials = #{client_id := _ClientId, client_secret := _ClientSecret}}} = application:get_env(zirconium_swallow, in),
    Credentials.

get_interval_ms() ->
    case application:get_env(zirconium_swallow, in) of
        % rate in ms
        {ok, #{rate := Rate}} when is_integer(Rate), Rate > 10000 ->
            Rate;
        % rate in specific unit
        {ok, #{rate := {Rate, RateUnit}}} ->
            erlang:convert_time_unit(Rate, RateUnit, millisecond);
        % invalid rate
        {ok, #{rate := _Other}} ->
            error("Invalid rate set, set either int millis > 10000, or {intrate, unit} (e.g. {120, second})");
        % no rate, use default
        _ ->
            erlang:convert_time_unit(120, second, millisecond)
    end.

init(State) ->
    % Register module with our own name, there's only ever going to be one poller
    true = register(?MODULE, self()),

    % check credentials are set
    get_creds(),
    % check that interval is valid
    get_interval_ms(),

    % trap exits from subprocesses, so we can see failures (returns previous value, should be false)
    false = process_flag(trap_exit, true),

    % start polling. I decided not to use a timer, because a high level of precision isn't
    % important, and it makes it a bit easier to vary the rate
    erlang:send_after(0, self(), poll),
    {ok, State}.

handle_info(poll, State) ->
    Credentials = get_creds(),
    RateMillis = get_interval_ms(),

    spawn_link(?MODULE, fetch, [Credentials]),
    erlang:send_after(RateMillis, self(), poll),

    {noreply, State};

% normal poller subproc exit, nothing to worry about
handle_info({'EXIT', _From, normal}, State) ->
    {noreply, State};

% poller subprocess failed
handle_info({'EXIT', From, Error}, State) ->
    logger:error("Non-normal EXIT from fetch job ~p: ~p", [From, Error]),
    zirconium_store ! {nr_lift, fail},
    {noreply, State};

handle_info({got, Results}, State) ->
    zirconium_store ! {nr_lift, Results},
    {noreply, State};

handle_info(_Message, _State) ->
    error("Unsupported message").

handle_call(_Message, _Sender, _State) ->
    error("Unsupported call").

handle_cast(_Message, _State) ->
    error("Unsupported cast").
