%%%-------------------------------------------------------------------
%% @doc zirconium_swallow public API
%% @end
%%%-------------------------------------------------------------------

-module(zirconium_swallow_app).

-behaviour(application).

-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
    zirconium_figure_1_stations:init_figure_1_stations(),
    logger:info("Waiting for mnesia tables..."),
    ok = mnesia:wait_for_tables([nr_lift], infinity),
    logger:info("Mnesia tables ready."),

    Port = case application:get_env(zirconium_swallow, out) of
        {ok, #{port := DefinedPort}} -> 
            DefinedPort;
        _ -> 60601
    end,

    Dispatch = cowboy_router:compile([
        {
            '_', [
               {"/", zirconium_handler_root, []},
               {"/station/:crs/lifts", zirconium_handler_station_lift, []}
            ]
        }
    ]),
    {ok, _} = cowboy:start_clear(zirconium_http_listener, [{port, Port}], #{
       env => #{dispatch => Dispatch},
       middlewares => [cowboy_router, cowboy_handler]
    }),
    zirconium_swallow_sup:start_link().

stop(_State) ->
    ok.

%% internal functions
