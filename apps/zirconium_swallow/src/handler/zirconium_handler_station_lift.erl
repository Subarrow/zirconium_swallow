-module(zirconium_handler_station_lift).

-include_lib("zirconium_swallow/include/nr_lift.hrl").

-export([init/2]).
-export([allowed_methods/2]).
-export([content_types_provided/2]).

-export([respond_json/2]).

init(Req, State) ->
    {cowboy_rest, Req, State}.

allowed_methods(Req, State) ->
    {[<<"GET">>], Req, State}. 

content_types_provided(Req, State) ->
    {[
        {<<"application/json">>, respond_json}
    ], Req, State}.

respond_json(Req, State) ->
    CRS = cowboy_req:binding(crs, Req),
    {atomic, Matches} = 
        mnesia:transaction(fun() ->
            mnesia:match_object(#nr_lift{_ = '_', station_crs = CRS})
            end),
    
    ResponseMap = #{
        result_count => length(Matches),
        network_rail_lifts => lists:map(fun zirconium_nr_lift_data:record_to_map/1, Matches),
        is_figure_1_station => zirconium_figure_1_stations:is_figure_1_station(CRS)
    },

    {jsone:encode(ResponseMap), Req, State}.