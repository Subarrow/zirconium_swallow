-module(zirconium_handler_root).

-behavior(cowboy_rest).

-export([init/2]).
-export([allowed_methods/2]).
-export([content_types_provided/2]).

-export([respond_json/2]).

init(Req, State) ->
    {cowboy_rest, Req, State}.

allowed_methods(Req, State) ->
    {[<<"GET">>], Req, State}. 

content_types_provided(Req, State) ->
    {[
        {<<"application/json">>, respond_json}
    ], Req, State}.

respond_json(Req, State) ->
    {"\"ok\"", Req, State}.