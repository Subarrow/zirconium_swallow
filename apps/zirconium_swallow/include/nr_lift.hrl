-record(nr_lift, {
    % block_id is pkey (forbidden for display)
    block_id,
    % permitted for display
    type, status, alternate_name, station_crs, station_name,
    
    % forbidden for display
    updated_time, operational_status, sensor_id, uprn, block_title, time_to_fix,

    % boolean flags (all prohibited by protocol)
    is_commission, is_customer, is_engineer_on_site, is_independent, is_isolated
    }).