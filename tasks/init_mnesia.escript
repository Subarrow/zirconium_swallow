#!/usr/bin/env escript
%% -*- erlang -*-
%%! -sname zirconium_swallow

-module(init_mnesia).
-export([main/1]).

-include("../apps/zirconium_swallow/include/nr_lift.hrl").

main([]) ->
    Nodes = [node()],
    ok = mnesia:create_schema(Nodes),
    ok = mnesia:start(),
    {atomic, ok} = mnesia:create_table(nr_lift, [
        {attributes, record_info(fields, nr_lift)},
        {index, [#nr_lift.station_crs, #nr_lift.sensor_id, #nr_lift.uprn, #nr_lift.is_commission, #nr_lift.is_customer, #nr_lift.type, #nr_lift.status, #nr_lift.operational_status, #nr_lift.updated_time]},
        {disc_copies, Nodes},
        {type, ordered_set}
    ]),
    stopped = mnesia:stop().
